
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
require('vue-resource');

import VueHighlightJS from 'vue-highlightjs'

window.Vue.use(VueHighlightJS)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Table from './components/Table.vue';
import Icon from './components/Icon.vue';
import Statistic from './components/Statistic.vue';

import Queues from './pages/Queues.vue';
import Jobs from './pages/Jobs.vue';

Vue.component('vue-table', Table);
Vue.component('icon', Icon);
Vue.component('statistic', Statistic);

Vue.component('queues', Queues);
Vue.component('jobs', Jobs);

const app = new Vue({
    el: '#app'
});
