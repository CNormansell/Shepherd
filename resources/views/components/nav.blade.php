<div class="header-wrapper">
  <div class="header container">
    <div class="logo visible-s-down text-right">
      <h3 class="display-title font-size-xl">
        <a href="{{ route('dashboard') }}" class="no-underline color-primary" tabindex="4">{{ config('app.name') }}</a>
      </h3>
    </div>

    <input type="checkbox" id="menu_toggle" class="hide">
    <label for="menu_toggle" class="header-button-container no-margin-vertical" tabindex="1" role="button">
      <div class="header-button">
        @icon('menu')
      </div>
    </label>

    <nav class="header-bar-container navigation-bar">
      <ul role="menu" class="header-bar">
        <li role="menuitem">
          <a href="{{ route('dashboard') }}" tabindex="2" class="{{ classNames([
            'link color-grey-100 no-underline',
            'active' => request()->routeIs('dashboard')
          ]) }}">
            Dashboard
          </a>
        </li>

        <li role="menuitem">
          <a href="{{ route('queues') }}" tabindex="2" class="{{ classNames([
            'link color-grey-100 no-underline',
            'active' => request()->is('queues*')
          ]) }}">
            Queues
          </a>
        </li>

        <li role="menuitem">
          <a href="" tabindex="2" class="{{ classNames([
            'link color-grey-100 no-underline',
            'active' => request()->is('dashboard')
          ]) }}">
            Workers
          </a>
        </li>
      </ul>
    </nav>

    <div class="logo visible-m-up">
      <h3 class="display-title font-size-xl">
        <a href="{{ route('dashboard') }}" tabindex="1" class="no-underline color-primary">{{ config('app.name') }}</a>
      </h3>
    </div>
  </div>
</div>
