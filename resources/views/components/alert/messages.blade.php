@if (session('error') !== null)
  @component('components.alert.error')
    {{ session('error') }}
  @endcomponent
@endif
@if (session('success', session('status')) !== null)
  @component('components.alert.success')
    {{ session('success', session('status')) }}
  @endcomponent
@endif
