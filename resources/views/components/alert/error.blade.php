<div class="background-error-300 color-white margin-bottom-xs padding-vertical-xs">
  <div class="container flex align-items-center">
    @icon('error', 'margin-right-xs')
    {!! $slot !!}
  </div>
</div>
