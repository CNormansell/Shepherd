<div class="background-success-300 color-white margin-bottom-xs padding-vertical-xs">
  <div class="container flex align-items-center">
    @icon('check_circle', 'margin-right-xs')
    {!! $slot !!}
  </div>
</div>
