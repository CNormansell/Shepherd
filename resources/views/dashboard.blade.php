@extends('layouts.master')

@section('content')
    <div class="container margin-auto">
        <div class="card">
            <div class="padding-xs border-bottom border-color-light-300">
                <h2>Overview</h2>
            </div>
            <div class="padding-xs flex flex-wrap">
                <statistic label="Number of Jobs" route="{{ route('api.statistics.jobs') }}"></statistic>
                <statistic label="Number of Workers" route="{{ route('api.statistics.workers') }}"></statistic>
                <statistic label="Number of Queues" route="{{ route('api.statistics.queues') }}"></statistic>
            </div>
        </div>
    </div>
@endsection
