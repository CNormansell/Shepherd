@extends('layouts.master')

@section('content')
    <div class="container margin-auto">
        <h2>Queues</h2>
        <div class="card">
            <div class="padding-xs flex flex-wrap justify-content-center">
                <queues style="width: 100%"></queues>
            </div>
        </div>
    </div>
@endsection
