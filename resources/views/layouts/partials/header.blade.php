<head>
  <meta charset="utf-8">
  <meta name="name" content="content">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name') }}</title>
  <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
  @routes
  <script>
    window.Laravel = { csrfToken: '{{ csrf_token() }}' };
  </script>

  @stack('head')
</head>
