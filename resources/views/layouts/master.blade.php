<!DOCTYPE html>
<html>
@include('layouts.partials.header')
<body>
<div id="app">
  <header role="header" class="margin-bottom-xs">
    @include('components.nav')
    @include('components.alert.messages')
  </header>
  @yield('content')
</div>

<script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
@stack('scripts')
</body>
</html>

