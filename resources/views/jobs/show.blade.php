@extends('layouts.master')

@section('content')
  <div class="container margin-auto">
    <h2>{{ request()->route('queue') }} Jobs</h2>
    <div class="card margin-bottom-m">
      <jobs queue="{{ request()->route('queue') }}" style="width: 100%"></jobs>
    </div>
  </div>
@endsection
