let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
.postCss('resources/assets/css/app.css', 'public/css').options({
  postCss: [
    require('postcss-omit-import-tilde'),
    require('postcss-import'),
    require('postcss-cssnext'),
    require('postcss-focus-ring'),
  ]
});
