<?php

use App\Http\Controllers\GetJobs;
use App\Http\Controllers\GetJobsForQueue;
use App\Http\Controllers\GetTotalJobs;
use App\Http\Controllers\GetTotalQueues;
use App\Http\Controllers\GetTotalWorkers;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('jobs', GetJobs::class)->name('api.jobs');
Route::get('jobs/{queue}', GetJobsForQueue::class)->name('api.jobs.show');

Route::prefix('statistics')->group(function () {
    Route::get('jobs', GetTotalJobs::class)->name('api.statistics.jobs');
    Route::get('workers', GetTotalWorkers::class)->name('api.statistics.workers');
    Route::get('queues', GetTotalQueues::class)->name('api.statistics.queues');
});

