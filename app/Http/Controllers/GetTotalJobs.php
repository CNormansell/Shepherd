<?php

namespace App\Http\Controllers;

class GetTotalJobs
{
    public function __invoke()
    {
        $queue = app('queue');
        $redisConnectionName = config('queue.connections.' . $queue->getConnectionName() . '.connection');
        $redisConnection = $queue->getRedis()->connection($redisConnectionName);
        $total = collect($redisConnection->eval(
                $this->allQueueNames(),
                1,
                'queues:*'
            ))
            ->map(function ($queueKeyName) {
                return preg_replace('/queues\:/', '', $queueKeyName);
            })
            ->reject(function ($queueName) {
                return ends_with($queueName, ':reserved') || ends_with($queueName, ':delayed');
            })
            ->map(function ($queueName) use ($queue) {
                return $queue->size($queueName);
            })
            ->sum();

        return response()->json($total);
    }

    public function allQueueNames()
    {
        return <<<'LUA'
return redis.call('keys', KEYS[1])
LUA;
    }
}
