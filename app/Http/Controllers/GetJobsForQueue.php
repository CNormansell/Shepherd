<?php

namespace App\Http\Controllers;

use hollodotme\IncompleteClassAccessor\IncompleteClassAccessor;
use Illuminate\Contracts\Redis\Factory;

class GetJobsForQueue
{
    public function __invoke(Factory $redis, $queueName)
    {
        $connection = $redis->connection();
        $queueKey = 'queues:' . $queueName;
        $jobs = collect($connection->eval($this->allItems(), 1, $queueKey))
            ->map(function ($json) {
                return json_decode($json);
            });

        $currentBlockName = null;
        $groupedJobs = collect();
        $currentBlock = collect();

        foreach ($jobs as $job) {
            if ($this->getBlockName($job) !== $currentBlockName && $currentBlockName !== null) {
                $groupedJobs->push((object) [
                    'name' => $currentBlockName,
                    'values' => $currentBlock
                ]);
                $currentBlock = collect();
            }

            $currentBlock->push($job);
            $currentBlockName = $this->getBlockName($job);
        }

        $groupedJobs->push((object) [
            'name' => $currentBlockName,
            'values' => $currentBlock
        ]);

        return response()->json($groupedJobs);
    }

    public function allItems()
    {
        return <<<'LUA'
return redis.call('lrange', KEYS[1], 0, -1)
LUA;
    }

    /**
     * @param $job
     * @return mixed
     */
    private function getBlockName($job)
    {
        if ($job->job === 'Illuminate\\Queue\\CallQueuedHandler@call') {
            $object = unserialize($job->data->command);
            $accessor = new IncompleteClassAccessor($object);

            return $accessor->getOriginalClassName();
        }

        return $job->job;
    }
}
