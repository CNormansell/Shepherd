<?php

namespace App\Http\Controllers;

class GetJobs
{
    public function __invoke()
    {
        $queue = app('queue');
        $redisConnectionName = config('queue.connections.' . $queue->getConnectionName() . '.connection');
        $redisConnection = $queue->getRedis()->connection($redisConnectionName);
        $queues = collect($redisConnection->eval(
                $this->allQueueNames(),
                1,
                'queues:*'
            ))
            ->map(function ($queueKeyName) {
                return preg_replace('/queues\:/', '', $queueKeyName);
            })
            ->reject(function ($queueName) {
                return ends_with($queueName, ':reserved') || ends_with($queueName, ':delayed');
            })
            ->map(function ($queueName) use ($queue) {
                return [
                    'name' => $queueName,
                    'total' => $queue->size($queueName),
                ];
            })
            ->values();

        return response()->json($queues);
    }

    public function allQueueNames()
    {
        return <<<'LUA'
return redis.call('keys', KEYS[1])
LUA;
    }
}
