<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Predis\Client;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Blade::directive('icon', function ($expression) {
            $exploded = explode(', ', $expression);
            $icon = array_get($exploded, 0);
            $classes = trim(array_get($exploded, 1), '\'');

            return "<?php echo '<i class=\"material-icons $classes\">' . $icon . '</i>'; ?>";
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Client::class, function ($app) {
            $client = new Client();

            return $client;
        });
    }
}
